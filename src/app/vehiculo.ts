export interface Vehiculos {
    Mark: string;
    Model: string;
    Type: string;
    Price: float;
    PLate: string;
  }
  