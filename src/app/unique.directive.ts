import { Directive, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appUnique]'
})
export class UniqueDirective implements OnInit {
  @Input() appUnique: string[];

  ngOnInit() {
    if (this.appUnique && this.appUnique.length) {
      const unique = [...new Set(this.appUnique.map(item => item[this.appUnique[0]]))];
      this.appUnique = unique.map(item => ({ [this.appUnique[0]]: item }));
    }
  }
}
