import { Component, OnInit } from '@angular/core';
import { Vehiculos } from './vehiculos';
import { VEHICULOS } from './vehiculos-data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Vehiculos App';
  vehiculos: Vehiculos[] = VEHICULOS;
  vehiculosFiltrados: Vehiculo[];
  tipoFiltrado = 'Todos';

  ngOnInit(): void {
    this.filtrarPorTipo();
  }

  filtrarPorTipo(): void {
    if (this.tipoFiltrado === 'Todos') {
      this.vehiculosFiltrados = this.vehiculos;
    } else {
      this.vehiculosFiltrados = this.vehiculos.filter(
        (vehiculos: Vehiculos) => vehiculos.Type === this.tipoFiltrado
      );
    }
  }
}
