import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'unique' })
export class UniquePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    const uniqueValues = [...new Set(value.map(item => item[args]))];
    return uniqueValues;
  }
}
